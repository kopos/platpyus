Setup
-----
```
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py createsuperuser
```


Running server
--------------
```
$ python manage.py runserver PORT
```
If PORT is not supplied, default port is 8000


Creating users
--------------
```
$ python manage.py runserver PORT
```
Go to http://localhost:PORT/admin/
Go to Users >> Add User


To login a user via API
-----------------------
Assuming a user is created with name and password is user1 and gt12356
respectively, to fetch a login auth token make a call to /api/login/
endpoint.

The sample curl call is below
```
$ curl -X POST -H 'Content-Type: application/json' -d "username=user1&password=gt123456" http://localhost:8000/api/login/
```

Docs & Sample
--------------
API calls are in gt/client.py

- to login & get token
- to get balance (info)
- to make debit
- to make credit
