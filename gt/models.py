from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class UserAccount(models.Model):
    user = models.OneToOneField(User, unique=True)
    bonus = models.FloatField(default=0)
    deposit = models.FloatField(default=0)
    winnings = models.FloatField(default=0)

    def __unicode__(self):
        return unicode(self.user)

User.profile = property(lambda u: UserAccount.objects.get_or_create(user=u)[0])


STATUSES = (
    (1, 'Init'),
    (2, 'Success'),
    (3, 'Fail'))


TRANSACTION_TYPES = (
    (1, 'Debit'),
    (2, 'Credit'))


class Transaction(models.Model):
    user = models.ForeignKey(UserAccount)
    gamecode = models.CharField(max_length=2)
    bonus_before = models.FloatField(default=0)
    deposit_before = models.FloatField(default=0)
    winnings_before = models.FloatField(default=0)
    trxn = models.PositiveSmallIntegerField(default=0, choices=TRANSACTION_TYPES)
    status = models.PositiveSmallIntegerField(default=0, choices=STATUSES)
    started_at = models.DateTimeField(auto_now_add=True)
    ended_at = models.DateTimeField(auto_now=True)
    bonus_sub = models.FloatField(default=0)
    deposit_sub = models.FloatField(default=0)
    winnings_sub = models.FloatField(default=0)
    bonus_later = models.FloatField(default=0)
    deposit_later = models.FloatField(default=0)
    winnings_later = models.FloatField(default=0)
