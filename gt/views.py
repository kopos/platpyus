from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import JsonResponse
from gt.models import Transaction


@api_view(['GET'])
def info(request):
    acct = request.user.profile
    return JsonResponse({
        'user_id': request.user.pk,
        'bonus': acct.bonus,
        'deposit': acct.deposit,
        'winnings': acct.winnings,
        'cash': acct.deposit + acct.winnings,
        'balance': acct.bonus + acct.deposit + acct.winnings,
        })


@api_view(['POST'])
def debit(request):
    acct = request.user.profile
    gc = request.POST.get('gameCode')
    mt = request.POST.get('moneyType', 'both')

    t = Transaction.objects.create(
            user_id=request.user.profile.pk,
            gamecode=gc,
            bonus_before=acct.bonus,
            deposit_before=acct.deposit,
            winnings_before=acct.winnings,
            status=1,
            trxn=1)
    tid = t.pk

    amt = float(request.POST.get('amount'))
    if ((mt == 'both' and amt > acct.bonus + acct.deposit + acct.winnings) or
        (mt == 'cash' and amt > acct.deposit + acct.winnings) or
        (mt == 'bonus' and amt > acct.bonus)):
        t.status = 3
        t.save(update_fields=['status'])
        return JsonResponse({'status': 'fail',
                             'reason': 'insufficient funds'},
                            status=412)

    bon_sub, dep_sub, win_sub = 0, 0, 0

    if mt == 'both':
        bon_sub = amt
        if bon_sub > acct.bonus:
            bon_sub = acct.bonus
            dep_sub = amt - bon_sub
            if dep_sub > acct.deposit:
                dep_sub = acct.deposit
                win_sub = amt - bon_sub - dep_sub
    elif mt == 'cash':
        dep_sub = amt
        if dep_sub > acct.deposit:
            dep_sub = acct.deposit
            win_sub = amt - bon_sub - dep_sub
    elif mt == 'bonus':
        bon_sub = amt

    try:
        assert(amt == (bon_sub + dep_sub + win_sub))
    except:
        t.status = 3
        t.save(update_fields=['status'])
        return JsonResponse({'status': 'fail',
                             'reason': 'server error'},
                            status=500)

    t.bonus_sub, t.bonus_later = bon_sub, t.bonus_before - bon_sub
    t.deposit_sub, t.deposit_later = dep_sub, t.deposit_before - dep_sub
    t.winnings_sub, t.winnings_later = win_sub, t.winnings_before - win_sub
    t.save()

    acct.bonus, acct.deposit, acct.winnings = t.bonus_later, t.deposit_later, t.winnings_later
    acct.save()

    return JsonResponse({
        'status': 'ok',
        'tid': tid,
        'info': {
            'bonus': t.bonus_later,
            'deposit': t.deposit_later,
            'winnings': t.winnings_later,
            'cash': t.deposit_later + t.winnings_later,
            'balance': t.bonus_later + t.deposit_later + t.winnings_later
            }})

@api_view(['POST'])
def credit(request):
    acct = request.user.profile
    gc = request.POST.get('gameCode')
    mt = request.POST.get('moneyType')

    t = Transaction.objects.create(
            user_id=request.user.profile.pk,
            gamecode=gc,
            bonus_before=acct.bonus,
            deposit_before=acct.deposit,
            winnings_before=acct.winnings,
            status=1,
            trxn=2)
    tid = t.pk

    amt = float(request.POST.get('amount', 0))
    bon_sub, dep_sub, win_sub = 0, 0, 0
    if amt > 0:
        tid0 = int(request.POST.get('tid'))
        t0 = Transaction.objects.get(pk=tid0)
        bet = t0.bonus_sub + t0.deposit_sub + t0.winnings_sub

        bon_sub = amt * (t0.bonus_sub / bet)
        dep_sub = amt * (t0.deposit_sub / bet)
        win_sub = amt * (t0.winnings_sub / bet)
    else:
        bon_sub, dep_sub, win_sub = map(lambda k: float(request.POST.get(k, 0)),
                                        ['bonus', 'deposit', 'winnings']) 

    t.status = 2
    t.bonus_sub, t.bonus_later = bon_sub, t.bonus_before + bon_sub
    t.deposit_sub, t.deposit_later = dep_sub, t.deposit_before + dep_sub
    t.winnings_sub, t.winnings_later = win_sub, t.winnings_before + win_sub
    t.save(update_fields=['status',
                          'bonus_sub', 'deposit_sub', 'winnings_sub',
                          'bonus_later', 'deposit_later', 'winnings_later'])

    acct.bonus, acct.deposit, acct.winnings = t.bonus_later, t.deposit_later, t.winnings_later
    acct.save()

    return JsonResponse({
        'status': 'ok',
        'tid': tid,
        'info': {
            'bonus': t.bonus_later,
            'deposit': t.deposit_later,
            'winnings': t.winnings_later,
            'cash': t.deposit_later + t.winnings_later,
            'balance': t.bonus_later + t.deposit_later + t.winnings_later
            }})
