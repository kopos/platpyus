import requests

# Login
res = requests.post(
        'http://localhost:8000/api/login/',
        data={'username': 'user1', 'password': 'gt123456'})
auth_token = 'JWT ' + res.json()[u'token']
print 'Received login auth token: %s' % auth_token

# Get balance info
res1 = requests.get(
        'http://localhost:8000/api/info/',
        headers={'Authorization': auth_token})
bal = res1.json()
print 'Received balance: %s' % bal

# Make debit request
res2 = requests.post(
        'http://localhost:8000/api/debit/',
        data={'amount': 30, 'gameCode': 'tp'},
        headers={'Authorization': auth_token})
trxn2 = res2.json()
if trxn2['status'] == 'ok':
    bal2 = trxn2['info']
    print 'TRXN_ID:%s | balance: %s' % (trxn2['tid'], bal2)
else:
    print 'ERROR: %s' % trxn2['reason']

# Make credit request
res3 = requests.post(
        'http://localhost:8000/api/credit/',
        data={'bonus': 10, 'deposit': 10, 'winnings': 10, 'gameCode': 'tp'},
        headers={'Authorization': auth_token})
trxn3 = res3.json()
if trxn3['status'] == 'ok':
    bal3 = trxn3['info']
    print 'TRXN_ID:%s | balance: %s' % (trxn3['tid'], bal3)
else:
    print 'ERROR: %s' % trxn3['reason']

# Make debit request
res4 = requests.post(
        'http://localhost:8000/api/debit/',
        data={'amount': 40, 'gameCode': 'tp'},
        headers={'Authorization': auth_token})
trxn4 = res4.json()
if trxn4['status'] == 'ok':
    bal4 = trxn4['info']
    print 'TRXN_ID:%s | balance: %s' % (trxn4['tid'], bal4)
else:
    print 'ERROR: %s' % trxn4['reason']

# Make credit request with amt
res5 = requests.post(
        'http://localhost:8000/api/credit/',
        data={'amount': 30, 'gameCode': 'tp', 'tid': trxn4['tid']},
        headers={'Authorization': auth_token})
trxn5 = res5.json()
if trxn5['status'] == 'ok':
    bal5 = trxn5['info']
    print 'TRXN_ID:%s | balance: %s' % (trxn5['tid'], bal5)
else:
    print 'ERROR: %s' % trxn5['reason']

# Make debit request
res6 = requests.post(
        'http://localhost:8000/api/debit/',
        data={'amount': 50, 'gameCode': 'tp', 'moneyType': 'cash'},
        headers={'Authorization': auth_token})
trxn6 = res6.json()
if trxn6['status'] == 'ok':
    bal6 = trxn6['info']
    print 'TRXN_ID:%s | balance: %s' % (trxn6['tid'], bal6)
else:
    print 'ERROR: %s' % trxn6['reason']

# Make credit request with amt
res7 = requests.post(
        'http://localhost:8000/api/credit/',
        data={'amount': 30, 'gameCode': 'tp', 'tid': trxn6['tid']},
        headers={'Authorization': auth_token})
trxn7 = res7.json()
if trxn7['status'] == 'ok':
    bal7 = trxn7['info']
    print 'TRXN_ID:%s | balance: %s' % (trxn7['tid'], bal7)
else:
    print 'ERROR: %s' % trxn7['reason']
