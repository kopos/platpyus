from django.conf.urls import include, url
from rest_framework_jwt.views import obtain_jwt_token
from gt import views as gtv


urlpatterns = [
    url(r'^login/', obtain_jwt_token),
    url(r'^info/', gtv.info),
    url(r'^debit/', gtv.debit),
    url(r'^credit/', gtv.credit)
]
