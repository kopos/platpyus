from django.contrib import admin
from .models import UserAccount, Transaction


class UserAccountAdmin(admin.ModelAdmin):
    list_display = ('user', 'bonus', 'deposit', 'winnings')
    readonly_fields = ('user',)


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'gamecode', 'status', 'trxn', 'started_at')

    def get_readonly_fields(self, request, obj=None):
       if obj:
           self.readonly_fields = [field.name for field in obj.__class__._meta.fields]
       return self.readonly_fields


admin.site.register(UserAccount, UserAccountAdmin)
admin.site.register(Transaction, TransactionAdmin)
