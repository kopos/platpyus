Django==1.10.5
djangorestframework==3.5.3
djangorestframework-jwt==1.9.0
PyJWT==1.4.2
requests==2.12.5
